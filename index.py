from flask import Flask, render_template, jsonify, request, abort
from routes.ui_kit import route as ui_kit

app = Flask(__name__)

app.register_blueprint(ui_kit, url_prefix='/ui_kit')

@app.route('/', methods=[ 'GET' ])
def index():
    return render_template('home.html'), 200

@app.route('/api/<string:action>/<int:key_one>.<int:key_two>', methods=[ 'POST' ])
def api(action, key_one, key_two):
    import one_time_pad

    if key_one <= 5 or key_two <= 5:
        return jsonify({
            'error': 'The keys must be greater to 5'
        }), 400

    if action != 'encrypt' and action != 'decrypt':
        return abort(404)

    if request.headers['Content-Type'].split(';')[0] == 'multipart/form-data':
        data = {}
        for key in request.form:
            data[key] = request.form[key]
    elif request.headers['Content-Type'].split(';')[0] == 'application/json':
        import json
        data = json.loads(request.data)
    else:
        return abort(400)

    if 'message' not in data:
        return abort(400)

    if action == 'encrypt':
        data['message'] = str(data['message'])
        data['message'] = data['message'].replace("'", '')
        data['message'] = data['message'].replace('"', '')
        data['message'] = data['message'].replace('"', '')
        data['message'] = data['message'].replace('<', '')
        data['message'] = data['message'].replace('>', '')
        data['message'] = data['message'].replace('{', '')
        data['message'] = data['message'].replace('}', '')
        data['message'] = data['message'].replace('[', '')
        data['message'] = data['message'].replace(']', '')
        data['message'] = data['message'].replace('´', '')
        data['message'] = data['message'].replace('`', '')
        data['message'] = data['message'].replace('^', '')
        data['message'] = data['message'].replace('\\', '')
        data['message'] = data['message'].replace('/', '')

        data['message'] = data['message'].replace('Á', 'A')
        data['message'] = data['message'].replace('À', 'A')
        data['message'] = data['message'].replace('Í', 'I')
        data['message'] = data['message'].replace('Ì', 'I')
        data['message'] = data['message'].replace('Ú', 'U')
        data['message'] = data['message'].replace('Ù', 'U')
        data['message'] = data['message'].replace('É', 'E')
        data['message'] = data['message'].replace('È', 'E')
        data['message'] = data['message'].replace('Ó', 'O')
        data['message'] = data['message'].replace('Ò', 'O')

        data['message'] = data['message'].replace('á', 'a')
        data['message'] = data['message'].replace('à', 'a')
        data['message'] = data['message'].replace('í', 'i')
        data['message'] = data['message'].replace('ì', 'i')
        data['message'] = data['message'].replace('ú', 'u')
        data['message'] = data['message'].replace('ù', 'u')
        data['message'] = data['message'].replace('é', 'e')
        data['message'] = data['message'].replace('è', 'e')
        data['message'] = data['message'].replace('ó', 'o')
        data['message'] = data['message'].replace('ò', 'o')

    data['message'] = data['message'].replace(' ', '~')
    data['len'] = len(data['message'])

    pad = one_time_pad.generate_pad(key_one, key_two, data['len'])

    if action == 'encrypt':
        res = one_time_pad.encode(pad, data['message'])
    else:
        res = one_time_pad.decode(pad, data['message']).replace('~', ' ')

    return jsonify(res)
    
    

if __name__ == "__main__":
    app.run()