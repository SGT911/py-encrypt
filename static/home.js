'use strict'

const elements = {
    message: document.getElementById('msg'),
    keyOne: document.getElementById('key-one'),
    keyTwo: document.getElementById('key-two'),
    action: document.getElementById('action'),
    run: document.getElementById('run'),
    output:  document.getElementById('out')
}

function onlyNumber(evt) {
    evt.target.value = evt.target.value.replace(/\D/g, '')
    evt.target.value = evt.target.value.replace(/^[0]+/g, '')
}

function onlyAscii(evt) {
    evt.target.value = evt.target.value.replace(/[\'\"\<\>\{\}\[\]\´\`\^\/\\]+/ig, '')

    evt.target.value = evt.target.value.replace(/[\á\à]/, 'a')
	evt.target.value = evt.target.value.replace(/[\í\ì]/, 'i')
	evt.target.value = evt.target.value.replace(/[\ú\ù]/, 'u')
	evt.target.value = evt.target.value.replace(/[\é\è]/, 'e')
    evt.target.value = evt.target.value.replace(/[\ó\ò]/, 'o')

    evt.target.value = evt.target.value.replace(/[\Á\À]/, 'A')
	evt.target.value = evt.target.value.replace(/[\Í\Ì]/, 'I')
	evt.target.value = evt.target.value.replace(/[\Ú\Ù]/, 'I')
	evt.target.value = evt.target.value.replace(/[\É\È]/, 'E')
	evt.target.value = evt.target.value.replace(/[\Ó\Ò]/, 'O')
}

elements.message.oninput = onlyAscii
elements.keyOne.oninput = onlyNumber
elements.keyTwo.oninput = onlyNumber

elements.run.onclick = async (evt) => {
    if (elements.message.value == '') {
        alert('The message is empty.')
        return false
    }

    if (elements.keyOne.value == '' || elements.keyTwo.value == '') {
        alert('The keys are empty.')
        return false
    }

    if (elements.keyOne.value <= 5 || elements.keyTwo.value <= 5) {
        alert('The keys are too littles.')
        return false
    }

    let res = await fetch(`/api/${elements.action.value}/${elements.keyOne.value}.${elements.keyTwo.value}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            message: elements.message.value
        }),
    }).then(out => out.json())

    if (typeof res == 'object' ) {
        if (res.hasOwnProperty('error')) {
            alert('Error :(')
            console.error(res)
            return false
        }

        alert('Response type un expected')
        console.info(res)
    }

    elements.output.innerText = `"${res}"`
}