from flask import Blueprint, Response
import requests

route = Blueprint('uikit', __name__)

@route.route('/<path:page>')
def uikit(page):
    url_download = f'https://cdn.jsdelivr.net/npm/uikit@3.4.6/dist/{page}'
    req = requests.get(url_download)
    res = Response(
        response=req.text,
        status=req.status_code,
        headers={
            'content-type': req.headers['content-type']
        }
    )

    return res