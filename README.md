# Py Encrypt

An encryption app using 2 keys

## Library
The library used is created with the concept "One time pad"
```python
import one_time_pad as otp

otp.CHARACTERS # Characters what the encryption algorithm Supports

otp.generate_pad # Generate a pad using 2 keys and a length

otp.encode # Encrypt function use a pad and a string
otp.decode # Decrypt function using the sames paramaters tha 'otp.encode'
```

# Deploy the server
```bash
pip install -r requirements.txt
gunicorns index:app
```
