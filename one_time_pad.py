#!/usr/bin/env python

import math

CHARACTERS = list(map(lambda x: x, 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz1234567890:;@#$%&¡!¿?_-+*~.,'))

def generate_pad(base1: int, base2: int, length: int) -> list:
	def get_numbers(x: int, y: int) -> str:
		if x > y:
			x, y = y, x
		return str( math.log(((x + 10) * 10) * math.e,((y + 10) * 10) * math.pi) )[2:]

	def divide(x: str) -> list:
		res = list()
		for i in range(0, len(x), 2):
			res.append(int(f'{x[i:i+2]}'))
		return res

	def extend(a: list, b: list) -> list:
		for el in b:
			a.append(el)
		return a

	nums = divide(get_numbers(base1, base2))

	while len(nums) < length:
		tmp = divide(get_numbers(nums[len(nums) - 2], nums[len(nums) - 1]))

		nums = extend(nums, tmp)

	return nums[:length]		

def encode(pad: list, message: str) -> str:
	def tranpose_letter(pos: int, char: str) -> str:
		MAX_POS = len(CHARACTERS) - 1

		if pos == 0:
			return char
		elif pos > 0:
			tmp = CHARACTERS.index(char) + 1
			if tmp > MAX_POS: 
				tmp = 0

			return tranpose_letter(pos - 1, CHARACTERS[tmp])
		else:
			tmp = CHARACTERS.index(char) - 1
			if tmp < 0: 
				tmp = MAX_POS

			return tranpose_letter(pos + 1, CHARACTERS[tmp])

	res = list()

	for i in range(len(message)):
		res.append(tranpose_letter(pad[i], message[i]))

	return ''.join(res)

def decode(pad: list, message: str) -> str:
	pad = list(map(lambda obj: obj * -1, pad))

	return encode(pad, message)

if __name__ == '__main__':
	import sys
	txt = sys.argv[1].replace(' ', '~')
	len_txt = len(txt)
	num_a = int(sys.argv[2])
	num_b = int(sys.argv[3])

	pad = generate_pad(num_a, num_b, len_txt)
	
	try:
		operation = sys.argv[4]
		if operation == 'E':
			txt_encoded = encode(pad, txt)
			print(f'"{txt_encoded}"')
		elif operation == 'D':
			txt_decoded = decode(pad, txt).replace('~', ' ')
			print(f'"{txt_decoded}"')
		elif operation == 'P':
			print(pad)
	except Exception as e:
		txt_encoded = encode(pad, txt)
		print("Pad => ",pad)
		print(f'Encoded => "{txt_encoded}"')

		txt_decoded = decode(pad, txt_encoded).replace('~', ' ')

		print(f'Decoded => "{txt_decoded}"')
